HTMLのフォームからファイルをアップロードする

# マークアップ

```html
<form action="./post.php" method="post" enctype="multipart/form-data">
  <p><input type="file" name="file-upload"></p>
  <p><input type="submit"></p>
</form>
```

## form要素のenctype属性

ファイルアップロード時には必須の属性。それ以外の場面では通常は省略可。ちなみに、省略時の初期値は`application/x-www-form-urlencoded`。

## input要素、button要素の`formenctype`属性

`input[type="submit"]`、`input[type="image"]`もしくは`button[type="submit"]`のとき、`formenctype`属性で同様の指定ができ、`form`の`enctype`よりも優先される。

## `input[type="file"]`で使える属性

### multiple

複数のファイルを送信を許可する。なお、`input[type="email"]`でも複数の値を許可するようになるらしい（使ったこと無いけど）。

### accept

サーバーが受け付けるファイルの種類を示す。値は拡張子かMIME Typeのカンマ区切り。

```html
<!-- 拡張子で指定 -->
<input type="file" accept=".jpg, .gif, .png">

<!-- MIME Typeで指定 -->
<input type="file" accept="image/jpeg, image/png, image/gif">
<input type="file" accept="image/*">
```

`accept`属性を記述すると、ファイル選択ボタンを押した時に表示されるファイル選択ダイアログで目的のファイル形式だけが表示されるようになる。 Constraint validation用の属性値ではないことに注意。

# JSでファイルのバリデーション

今回は画像ファイルの場合を想定します。

## 選択されたファイル情報の取得

* [FileList](https://developer.mozilla.org/ja/docs/Web/API/FileList)
* [File](https://developer.mozilla.org/ja/docs/Web/API/File)

対象の`input`要素の`files`プロパティにFileListオブジェクトが得られます。FileListには複数のFileオブジェクトを内包しているので、繰り返し処理で一つ一つのファイル情報を参照する感じになります。

```js
$('input[type="file"]).change(function() {
    console.log($(this)[0].files);
});
```

## ファイルタイプは適切か

指定されたファイルに適切な拡張子がついていれば、`type`プロパティからMIMEタイプを参照できます。

ただし、拡張子は常に適切とは限らないので、この値を過信するのは危険です。

## ファイルのサイズは適切か

`size`プロパティでファイルサイズを確認できます。単位はbyte。アプリケーションの仕様と比較して大きすぎたり小さすぎたりしないか確認できます。

## `window.URL.createObjectURL`を利用した検証

* [window.URL.createObjectURL](https://developer.mozilla.org/ja/docs/Web/API/URL/createObjectURL)

`window.URL.createObjectURL`メソッドでFileオブジェクトからURLを作成することができます。オブジェクトURLと呼びます。

この仕組みを使うともうすこし踏み込んだ検証もできるようになります。

### 真・ファイルタイプは適切か

`<img>`要素のsrc属性にオブジェクトURLを指定し、エラーにならないことが確認できれば、少なくともブラウザで扱うことのできる画像形式であることが確認できます。

```js
$('input[type="file"]).change(function() {
  var file = $(this)[0].files[0];
  var objectUrl = URL.createObjectURL(file);
  var $img = $('<img>', {src: objectUrl});
  $img.on(
    'error',
    function(e) {
      // 何らかのエラー処理
      console.log(e);
    }
  );
});
```

### 画像のアスペクト比やピクセル数は適切か

* [HTMLImageElement](https://developer.mozilla.org/ja/docs/Web/API/HTMLImageElement)

上の例で生成した`<img>`のプロパティを調べればwidth、heightなどを調べられます。

```js
$img.on(
  'load',
  function() {
    console.dir(this);
  }
);
```

# 検証結果をユーザーに知らせる

* [Constraint validation](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5/Constraint_validation)
* [データフォームの検証](https://developer.mozilla.org/ja/docs/Web/Guide/HTML/Forms/Data_form_validation)

## CSS

擬似クラス`:valid`、`:invalid`でスタイルを指定できる。

# 実際に送信されるデータを観察してみる

ChromeのDevToolsのNetworkタブで通信の詳細を観察できる。

enctypeなしの場合は、ファイル名のみが送信される。

```text
file-upload=OI000083.jpg
```

`enctype="multipart/form-data"`の場合は以下のようになる。実際のデータでは、行の区切りはCR+LF。DevToolsの表示ではアップロードされたファイルの内容そのものは省略されているが、実際には各Boundaryのヘッダ部に続けて、ボディ部でファイルのデータを送信している。

```text
------WebKitFormBoundaryl3V844hh7BFebltm
Content-Disposition: form-data; name="file-upload"; filename="OI000083.jpg"
Content-Type: image/jpeg


------WebKitFormBoundaryl3V844hh7BFebltm--
```

# サーバーサイド(PHP)

`enctype="multipart/form-data"`でファイルを受け取った時、PHPのグローバル変数`$_FILES`にファイル情報が格納される。

```php
<?php
var_dump($_FILES);
```

```text
array (size=1)
  'file-upload' => 
    array (size=5)
      'name' => string 'OI000083.jpg' (length=12)
      'type' => string 'image/jpeg' (length=10)
      'tmp_name' => string '/tmp/php6yUgMh' (length=14)
      'error' => int 0
      'size' => int 927347
```

`multipart`を有効にして複数のファイルを受信した時の`$_FILES`は以下のような感じになる。

```text
array (size=1)
  'file-upload' => 
    array (size=5)
      'name' => 
        array (size=2)
          0 => string 'OI000083.jpg' (length=12)
          1 => string 'PB013186.JPG' (length=12)
      'type' => 
        array (size=2)
          0 => string 'image/jpeg' (length=10)
          1 => string 'image/jpeg' (length=10)
      'tmp_name' => 
        array (size=2)
          0 => string '/tmp/phpgo3N33' (length=14)
          1 => string '/tmp/phpdZw19C' (length=14)
      'error' => 
        array (size=2)
          0 => int 0
          1 => int 0
      'size' => 
        array (size=2)
          0 => int 927347
          1 => int 1021797
```

## $_FILESの仕様

| キー     | 説明                                                                                                                                                                                   |
|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| name     | クライアントマシンの元のファイル名。                                                                                                                                                   |
| type     | ファイルの MIME 型。ただし、ブラウザがこの情報を提供する場合。 例えば、"image/gif" のようになります。 この MIME 型は PHP 側ではチェックされません。そのため、 この値は信用できません。 |
| size     | アップロードされたファイルのバイト単位のサイズ。                                                                                                                                       |
| tmp_name | アップロードされたファイルがサーバー上で保存されているテンポラリファイルの名前。                                                                                                      |
| error    | このファイルアップロードに関する エラーコード                                                                                                                                          |

errorに0以外の値が入っていたら全部エラー。

tmp_nameは文字通り一時的なファイルで、PHPの処理が終了すると自動で削除される（明示的に削除するコードを書く必要はない）。

## アップロードされたファイルのバリデーション

アプリケーションが要求するファイル形式によって、必要なバリデーションの内容は当然変わる。ここでは画像をアップロードする場合に注意すべき点を確認する。

### ファイルタイプの判定

`$_FILES`に格納される`type`は、ブラウザが送信するものなので信用できない。多くのブラウザの実装では、拡張子からtypeを判別している。実際のファイル形式と拡張子が一致している保証はないので[Fileinfo](http://php.net/manual/ja/book.fileinfo.php)などで判定する。

画像かどうかだけを確認したい場合は、汎用の`finfo`を使うよりも、もっと手軽にPHPの画像処理関連の関数`getimagesize()`や`exif_imagetype()`などを使う方法もある。戻り値がfalseのとき、画像ファイルではない（もしくはPHPが対応していない画像形式）とわかる。

### ファイル容量、画像サイズ、アスペクト比の判定

最小サイズ、最大サイズのしきい値を事前に設定して、外れた場合にどのような処理をするのかを決めておくこと。最近の実装では、エラーにはせず、アプリケーションの仕様に合うようにPHP側で加工してしまうことがおおい。

#### MAX_FILE_SIZEは書くべき？

PHPの[マニュアル](http://php.net/manual/ja/features.file-upload.post-method.php)には`<input type="hidden" name="MAX_FILE_SIZE" value="30000" />`のようなフィールドを常に指定しておくべき、としている。理由としては<q>巨大なファイルを転送しようとして長時間待ち続け、 結局ファイルが大きすぎて転送できなかったなどといった羽目に陥るのを防げるからです。 </q>と、書いてあるけれども、これはちょっと良くわからない。ブラウザはこのフィールドがあろうとなかろうとサーバーにファイルを完全に転送しようとするので、待ち時間は同じ。

MAX_FILE_SIZEフィールドがある場合、PHPは`tmp_name`にファイルを保存せずに`error`フィールドに`UPLOAD_ERR_FORM_SIZE`を入れる。一時ファイルの生成を飛ばしているので、サーバー側のリソースの節約になることは期待できる。
