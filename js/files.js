/**
 * Fileの検証
 */
$(function() {
  $('input[type="file"]').change(function() {
    var files = $(this).prop('files');
    $.each(files, function() {
      var url = URL.createObjectURL(this);
      console.log(url);
      var $img = $('<img>', {src: url});
      $img.on(
        'error',
        function(e) {
          console.log(e);
        }
      );
      $img.on(
        'load',
        function() {
          console.dir(this);
        }
      );
    });
  });
});
